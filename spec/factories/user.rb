FactoryBot.define do
  
  factory User do
    email { "example@example.com" }
    password { "password" }
  end
end