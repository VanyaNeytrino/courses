FactoryBot.define do
  
  factory Group do
    title { "Web Dev" }
    start_date { Time.now + 5.day }
    status { "recruitment_participants" }
  end
end