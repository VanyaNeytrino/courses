FactoryBot.define do
  
  factory Course do
    title { "Web Dev" }
    description { "Web development" }
  end
end