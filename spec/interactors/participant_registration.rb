require 'rails_helper'

RSpec.describe ParticipantRegistration do
  let(:user) { create(:user) }
  let(:course) { create(:course) }
  let(:group) { create(:group, course: course) }
  let(:groups_params) { { id: group.id, email: user.email } }

  it '.call should create participant' do
    result = ParticipantRegistration.call(
      group_id: groups_params[:id],
      email: groups_params[:email]
    )
    expect(result).to be_success
  end

  context '.call should create participant' do
    let(:user) { create(:user) }
    let(:course) { create(:course) }
    let(:group) { create(:group, course: course) }
    let(:groups_params) { { id: group.id, email: user.email } }

    it 'return error: Юзер не найден' do
      groups_params[:email] = ''

      result = ParticipantRegistration.call(
        group_id: groups_params[:id],
        email: groups_params[:email]
      )
      expect(result.success?).to eq false
      expect(result.message).to eq 'Юзер не найден'
    end

    it 'return error: Группа не найдена' do
      group.delete

      result = ParticipantRegistration.call(
        group_id: groups_params[:id],
        email: groups_params[:email]
      )
      expect(result.success?).to eq false
      expect(result.message).to eq 'Группа не найдена'
    end

    it 'return error: Набор закрыт' do
      group.status = 'started'
      group.save && group.reload

      result = ParticipantRegistration.call(
        group_id: groups_params[:id],
        email: groups_params[:email]
      )
      
      expect(result.success?).to eq false
      expect(result.message).to eq 'Набор закрыт'
    end

    it 'return error: Юзер уже участник курса' do
      participant = Participant.create(user_id: user.id, group_id: group.id)

      result = ParticipantRegistration.call(
        group_id: groups_params[:id],
        email: groups_params[:email]
      )
      
      expect(result.success?).to eq false
      expect(result.message).to eq 'Юзер уже участник курса'
    end
  end
end