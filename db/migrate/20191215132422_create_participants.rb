class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.belongs_to :group
      t.belongs_to :user
      t.datetime   :join_to_group_date

      t.timestamps
    end
  end
end
