class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.belongs_to :course
      t.string     :title
      t.datetime   :start_date
      t.string     :status     

      t.timestamps
    end
  end
end
