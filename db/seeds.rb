# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
  Course.create!(title: 'Web Разюработка', description: 'Основы Web разработки')
  Group.create!(title: 'Группа курса веб разработки', start_date: Date.today + 5.day, status: 'recruitment_participants')
  Course.create!(title: 'IOS разработка', description: 'Основы iOS dev')
  Group.create!(title: 'Группа курса iOS разработки', start_date: Date.today + 6.day, status: 'recruitment_participants')
  User.create!(email: 'user@example.com', password: 'password')

end