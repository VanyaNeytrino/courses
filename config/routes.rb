Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root to: "home#index"
  devise_for :users

  resources :groups, only: [:show] do
  end

  post '/sign_up', to: 'groups#sign_up', as: :group_sign_up
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
