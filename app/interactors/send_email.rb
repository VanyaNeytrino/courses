# frozen_string_literal: true

class SendEmail
  include Interactor

  def call
    # вызов ассинхронного воркера
    context.success!
  end
end
