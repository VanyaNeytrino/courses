# frozen_string_literal: true

class Validate
  include Interactor

  def call
    user = User.find_by(email: context.email)
    group = Group.find_by_id(context.group_id)

    return context.fail!(message: 'Юзер не найден') unless user
    return context.fail!(message: 'Группа не найдена') unless group
    return context.fail!(message: 'Набор закрыт') if group_valid?(group)
    return context.fail!(message: 'Юзер уже участник курса') if user_registered?(user, group)

    context.user = user
  end

  private

  def group_valid?(group)
    group.started? || group.closed?
  end

  def user_registered?(user, group)
    return unless group
    
    group.participants.pluck(:user_id).include?(user.id)
  end
end
