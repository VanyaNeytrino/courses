# frozen_string_literal: true

class ParticipantRegistration
  include Interactor::Organizer

  organize Validate, Register, SendEmail
end
