# frozen_string_literal: true

class Register
  include Interactor

  def call
    return context.fail!(message: create_user) unless create_user == true
  end

  private

  def create_user
    participant = Participant.new(user_id: context.user.id, group_id: context.group_id)
    participant.valid? ? participant.save : participant.errors.full_messages
  end
end
