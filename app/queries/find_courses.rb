class FindCourses
  attr_accessor :params

  def initialize(params)
    @params = params
  end

  def call
    courses = all_courses
    courses = sort_by_date(courses) if params[:sort_by_data].present?
    courses
  end

  private

  def all_courses
    @all_courses ||= Course.left_outer_joins(groups: :participants)
                           .where('groups.status = ?', 'recruitment_participants')
                           .select('courses.id,
                                   courses.created_at as created_at, 
                                   courses.title as title,
                                   groups.id as group_id,
                                   groups.start_date as start_date, 
                                   count(participants) as total_participants')
                           .group('courses.id, courses.created_at, courses.title, 
                                   groups.id, groups.start_date')

  end

  def sort_by_date(courses)
    courses.order('start_date')
  end
end