ActiveAdmin.register Group do

  permit_params :course_id, :title, :start_date, :status

  index do
    selectable_column
    id_column
    column :course_id
    column :title
    column :start_date, as: :datepicker
    column :created_at
    actions
  end

  filter :title
  filter :created_at

  form do |f|
    f.inputs do
      f.select :course_id, Course.pluck(:title, :id)
      f.input :title
      f.input :start_date, as: :datepicker
      f.select :status, Group::STATUSES
    end
    f.actions
  end
end
