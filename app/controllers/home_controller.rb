# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @courses = FindCourses.new(permited_params).call
  end

  private

  def permited_params
    params.permit(:sort_by_data)
  end
end
