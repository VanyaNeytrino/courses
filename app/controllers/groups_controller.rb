# frozen_string_literal: true

class GroupsController < ApplicationController
  before_action :find_group, only: :show

  def show; end

  def sign_up
    result = ParticipantRegistration.call(
      group_id: groups_params[:id],
      email: groups_params[:email]
    )
    if result.success?
      redirect_to root_path, notice: 'Вы зарегистрированы в группе'
    else
      redirect_to root_path, notice: "Ошибка: #{result.message}"
    end
  end

  private

  def groups_params
    params.require(:group).require(:users).permit(:id, :email)
  end

  def find_group
    @group = Group.find(params[:id])
  end
end
