class Group < ApplicationRecord
  STATUSES = %w[started recruitment_participants closed].freeze

  belongs_to :course
  has_many :participants
  has_many :users, through: :participants

  validates :title, :course_id, :start_date, :status, presence: true

  scope :recruitment_participants, -> { where(status: 'recruitment_participants') }

  def started?
    status == 'started'
  end

  def closed?
    status == 'closed'
  end
end
